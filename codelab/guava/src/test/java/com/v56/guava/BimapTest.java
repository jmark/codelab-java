package com.v56.guava;

import org.junit.Test;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class BimapTest {

	@Test
	public void inverseTest() {
		BiMap<Integer, String> logfileMap = HashBiMap.create();
		logfileMap.put(1, "a.log");
		logfileMap.put(2, "b.log");
		logfileMap.put(3, "c.log");
		System.out.println("logfileMap:" + logfileMap);
		
		
		BiMap<String, Integer> filelogMap = logfileMap.inverse();
		System.out.println("filelogMap:" + filelogMap);
	}
}
