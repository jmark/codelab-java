/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.guava.base;

import com.google.common.base.Preconditions;

/**
 * <p>
 * Description: 让方法调用的前置条件判断更简单
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年11月1日
 */
public class PreconditionsTest {
	public static void main(String[] args) {
		Preconditions.checkArgument(1<2);
		Preconditions.checkNotNull("");
	}
	
	
}
