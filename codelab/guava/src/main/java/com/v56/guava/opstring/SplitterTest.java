/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.guava.opstring;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;

/**
 * <p>
 * Description: Guava分割器
 * </p>
 * 
 * @author majintao
 * @version 1.0
 * @Date 2015年11月7日
 */
public class SplitterTest {

	public static void main(String[] args) {
		String str = "a,,b,     c,,,,,,,,d,t++,dd1,dd3,xx4";
		Iterable<String> result = Splitter.on(",")
				.trimResults(CharMatcher.DIGIT).omitEmptyStrings().split(str);
		System.out.println("==Start==");
		for (String s : result) {
			System.out.println(s);
		}
		System.out.println("==End==");
	}
}
