/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.guava.Immutable;

import com.google.common.collect.ImmutableSet;

/**
 * <p>
 * Description: 
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年11月7日
 */
public class ImmutableTest {
	public static void main(String[] args) {
		testImmutable();
	}
	
	public static void testImmutable() {
		ImmutableSet<String> COLOR_NAMES = ImmutableSet.of(
		        "red",
		        "orange",
		        "yellow",
		        "green",
		        "blue",
		        "purple");
		COLOR_NAMES.asList();
		System.out.println("asList Test:" + COLOR_NAMES.asList());
	}
}
