/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.guava.base;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.v56.common.model.Point;

/**
 * <p>
 * Description: Guava基本用法测试
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年11月1日
 */
public class BaseTest {
	public static void main(String[] args) {
		testOptional1();
//		testOptional2();
//		testOptional3();
//		testObjects1();
//		testObjects2();
	}
	

	public static void testOptional1() {
		Optional<String> possible = Optional.of("aaa");
		System.out.println(possible.isPresent());
		System.out.println(possible.get());
		System.out.println(possible.asSet());
		
	}
	
	public static void testOptional2() {
		Optional<String> possible = Optional.absent();
		System.out.println(possible.isPresent());
	}
	
	public static void testOptional3() {
		String app = null;
		Optional<String> possible = Optional.fromNullable(app);
		System.out.println(possible.isPresent());
		System.out.println(possible.or("app"));
	}
	
	public static void testObjects1() {
		String app1 = null;
		String app2 = "qq"; 
//		System.out.println(Objects.firstNonNull(app1, app2));
		System.out.println(Strings.emptyToNull(app2));
		System.out.println(Strings.isNullOrEmpty(""));
	}
	
	public static void testObjects2() {
		Point point = new Point();
		System.out.println(point.toString());
		System.out.println(Objects.toStringHelper(point).add("x",2).toString());
	}
	
	
}
