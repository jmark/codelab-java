/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package org.gson;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

/**
 * <p>
 * Description: Gson 测试
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年10月26日
 */
public class GsonTest {
	
	private static final Logger logger = LoggerFactory.getLogger(GsonTest.class);
	private static Gson gson = new Gson();
	
	@Test
	public void testMapParamInteger() {
		Map map = new HashMap();
		map.put("userId", "uuid");
		map.put("level",1);
		logger.info(gson.toJson(map));
	}
	
	public static void main(String[] args) {
		Map map = new HashMap();
		map.put("userId", "uuid");
		map.put("level",1);
		System.out.println(gson.toJson(map));
	}
}
