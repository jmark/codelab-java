/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.other;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * <p>
 * Description: 
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年10月9日
 */
public class SubListTest {

	@Test
	public void test() {
		List<Integer> test = new ArrayList<Integer>();
		test.add(1);
		test.add(2);
		test.add(3);
		
		for(Integer i : test.subList(5, 1)) {
			System.out.println(i);
		}
	}
}
