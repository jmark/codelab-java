/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.other;

import org.junit.Test;

import com.v56.common.EnumTest;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author majintao
 * @version 1.0
 * @Date 2015年11月8日
 */
public class EnumExample {

	public enum Gender {
		// 通过括号赋值,而且必须带有一个参构造器和一个属性跟方法，否则编译出错
		// 赋值必须都赋值或都不赋值，不能一部分赋值一部分不赋值；如果不赋值则不能写构造器，赋值编译也出错
		MAN("MAN"), WOMEN("WOMEN");

		private final String value;

		// 构造器默认也只能是private, 从而保证构造函数只能在内部使用
		Gender(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	@Test
	public void test() throws Exception {
		Gender gender = Gender.MAN;
		System.out.println(gender.ordinal());
		System.out.println(gender.getValue());
		
		for (EnumTest e : EnumTest.values()) {
			System.out.println(e.toString());
			System.out.println(e.ordinal());
		}

		System.out.println("----------------我是分隔线------------------");

		EnumTest test = EnumTest.TUE;
		switch (test) {
		case MON:
			System.out.println("今天是星期一");
			break;
		case TUE:
			System.out.println("今天是星期二");
			break;
		// ... ...
		default:
			System.out.println(test);
			break;
		}
	}
}
