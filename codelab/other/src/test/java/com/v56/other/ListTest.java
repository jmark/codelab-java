/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.other;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Description: 
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2016年2月16日
 */
public class ListTest {
	
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		for(int i = 90; i <= 100; i ++) {
			list2.add(String.valueOf(i));
		}
		
		for(int i = 1; i <= 120; i ++) {
			list.add(String.valueOf(i));
		}
		
		System.out.println(list2);
		System.out.println(list);
		System.out.println(System.currentTimeMillis());
		for(String id2 : list2) {
			for(String id : list) {
				if(id.equals(id2)) {
					list.remove(id);
					break ;
				}
			}
		}
		System.out.println(System.currentTimeMillis());
		System.out.println(list);
	}
}
