/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.common;

/**
 * <p>
 * Description: 枚举测试类
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年11月8日
 */
public enum EnumTest {
	MON, TUE, WED, THU, FRI, SAT, SUN;
}
