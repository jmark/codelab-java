/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.innerOuterTest;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author majintao
 * @version 1.0
 * @Date 2016年2月6日
 */


public class Parcel4 {
	
	public interface Destination {
		public String readLabel();
	};

	public Destination destination(String s) {
		class PDestination implements Destination {
			private String label;

			private PDestination(String whereTo) {
				label = whereTo;
			}

			public String readLabel() {
				return label;
			}
		}
		return new PDestination(s);
	}

	public static void main(String[] args) {
		Parcel4 p = new Parcel4();
		Destination d = p.destination("Tasmania");
		System.out.println(d.readLabel());
	}
}
