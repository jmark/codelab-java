/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.innerOuterTest;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author majintao
 * @version 1.0
 * @Date 2016年2月6日
 */
public class Parcel5 {

	private void internalTracking(boolean b) {
		if (b) {
			class TrackingSlip {
				private String id;

				TrackingSlip(String s) {
					id = s;
				}

				String getSlip() {
					return id;
				}
			}
			TrackingSlip ts = new TrackingSlip("slip");
			String s = ts.getSlip();
		}
		
	}

	public void track() {
		internalTracking(true);
	}

	public static void main(String[] args) {
		Parcel5 p = new Parcel5();
		p.track();
	}
}
