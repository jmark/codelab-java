/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.innerOuterTest;

/**
 * <p>
 * Description: 
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2016年2月6日
 */
public class Outer2 { 
    public static void main(String[] args) { 
        Outer2 outer = new Outer2(); 
        Inner inner = outer.getInner("Inner", "gz"); 
        System.out.println(inner.getName()); 
    } 
 
    public Inner getInner(final String name, String city) { 
        return new Inner() { 
            private String nameStr = name; 
 
            public String getName() { 
                return nameStr; 
            } 
        }; 
    } 
} 
 
//注释后，编译时提示类Inner找不到 
interface Inner { 
    String getName(); 
} 
