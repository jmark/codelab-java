/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.v56.netty.example.echo;

import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

/**
 * <p>
 * Description: 
 * </p>
 * @author majintao
 * @version 1.0
 * @Date 2015年11月7日
 */
public class EchoServerHandler extends ChannelHandlerAdapter { // (1)
	 

    public void channelRead(ChannelHandlerContext ctx, Object msg) { // (2)
    
       ctx.write(msg); // (1)

       ctx.flush(); // (2)

    }

 

    @Override

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)

       // Close theconnection when an exception is raised.

       cause.printStackTrace();

       ctx.close();

    }

}
