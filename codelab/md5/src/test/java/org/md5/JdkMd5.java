/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package org.md5;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

/**
 * <p>
 * Description: JDK自带MD5加密
 * </p>
 * 
 * @author majintao
 * @version 1.0
 * @Date 2015年7月7日
 */
public class JdkMd5 {

    @Test
    public void test() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String name = "name";
        String passwd = "password";
        Random rand = new Random();
        byte[] salt = new byte[12];
        rand.nextBytes(salt);
        String ssalt = Arrays.toString(salt);
        System.out.println(salt);
        System.out.println(ssalt);
        System.out.println(ssalt.getBytes());
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(salt);
        
        m.update(passwd.getBytes("UTF8"));
        byte s[] = m.digest();
        String result = "";
        for (int i = 0; i < s.length; i++) {
            result += Integer.toHexString((0x000000ff & s[i]) | 0xffffff00).substring(6);
        }
        System.out.println(name);
        for (int i = 0; i < salt.length; i++) {
            System.out.print(salt[i] + ",");
        }

        System.out.println();
        System.out.println(result);
    }
}
