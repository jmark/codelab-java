/**
 * Copyright (c) 2015 Sohu. All Rights Reserved
 */
package com.codelab.netty.chatdemo.bootstrap;

import com.codelab.netty.chatdemo.listener.CharteventListener;
import com.codelab.netty.chatdemo.model.ChatObject;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.Transport;

/**
 * <p>
 * Description:引导启动程序
 * </p>
 * 
 * @author majintao
 * @version 1.0
 * @Date 2015年10月31日
 */
public class StartServer {
	public static void main(String[] args) throws InterruptedException {
		Configuration config = new Configuration();
		config.setHostname("localhost");
//		config.setTransports(Transport.WEBSOCKET);
		config.setPort(9092);

		SocketIOServer server = new SocketIOServer(config);
		CharteventListener listner = new CharteventListener();
		listner.setServer(server);
		// chatevent为事件名称
		server.addEventListener("chatevent", ChatObject.class, listner);
		// 启动服务
		server.start();
		Thread.sleep(Integer.MAX_VALUE);
		server.stop();
	}
}
