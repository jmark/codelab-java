package com.v56.json;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.alibaba.fastjson.JSON;

public class JsonTest {

	@Test
	public void testMap() {

		Map<String, String> map = new HashMap<String, String>();
		map.put("a", "pp");
		map.put("b", "qq");
		map.put("c", "gg");
		
		String json = JSON.toJSONString(map, true);
		System.out.println(json);
		
		// JSON -> Map
		Map<String, String> map1 = (Map<String, String>) JSON.parse(json);
		
		for (String key : map1.keySet()) {
			System.out.println(key + ":" + map1.get(key));
		}
	}
}
